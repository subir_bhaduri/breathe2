**Breathe2**

Here's an opensource air pollution monitoring device for measuring PM levels and sending data through GPRS to cloud (ThingSpeak platform).
For more regular updates see: http://smalldesign.in/category/breathe2/

**Why? Why another one!!!**

True, it could as well have been named YAAPMS for Yet Another Air Pollution Monitoring System. While many low cost air pollution measuring devices are available both commercially and through opensource, I didn't have all the details in a single place to begin with, when I wanted to create my own. Here are some limitations I could see of the existing ones:
* Many available devices connect a low cost Chinese PM sensor to an Arduino or ESP8266 or some more difficult MCU and make a cloud connection through **WiFi**. So where's the problem? 
    * Chinese PM sensors do not have adequate documentation of calibration procedures, life etc. They may also not provide technical support when needed. 
    * WiFi is not so openly and commonly available, at least in India.
* The few devices available in India that use the GPRS (mobile SIM) systems cost too much. (Airveda is an example).
* Many are not built for being placed in harsh external environments with rain, winds and temperatures commonly found in India.
* Commercial devices suffer from lack of transparency of device internals, calibration procedure, data reporting methods, etc. 
* Commercial devices host data on propriety servers/cloud and may provide only part of the data, say only of the last month.
* Even if provided, i do not know of anyone who gives this data in 'analysis-friendly form', like a .csv file. 
Of course commercial devices would provide better services, repair broken systems and so on, the above clearly shows where they lack. Hence the opensource *Breathe2* for citizen science purposes.

**OK, but what's different in *Breathe2*?**
* Open source hardware and firmware.
* Uses SPS30, a costlier and better designed PM sensor from an established sensor company Sensirion AG, Switzerland.     
* Uses Wemos D1 Mini (ESP8266) as MCU and a SIM800L module : gives a powerful combo of processing power and connectivity of WiFi as well as GPRS (2G networks).     
* Has an on-board temperature, pressure and relative humidity sensor (Bosch's fantastic BME280).     
* Sends all data to free publicly accessible channels on ThingSpeak.com. All data can be downloaded in csv format, from inception to present, by anyone!    
* Has a powerful 12VDC fan so that it has high local air suction, seems to provide high sensitivity in large physical areas such as outdoors.     
* Except electronics, uses commonly available PVC piping as structure, which keeps cost low.     
* Electronic components are all through hole and easy to find (except SPS30), so expertise required for full assembly is minimal (soldering).     
* Being opensource, it is encouraged to hack the current version and make better or adapt to other needs.

**Structure of this repo**
* PCB- PCB design files made in KiCAD V5+ and contains, schematics and PCB of the electronics, Bill of Materials and Gerbers for those who wish to directly fabricate one and so on. A wiki on the explanation of the schematics is due.    
* Firmware- Various files for coding the *WeMos D1 Mini* using Arduino IDE + Explanations (due sometime soon)
* Physical setup (due)- Pictures, explanations and assembly plan  in a single document.
* Thingspeak channel configurations (due)

**Team**
* Abhijit Savant (MS student of Industrial Design at Arizona State University, USA.)
* Dr. Sumithra Surendralal (Symbiosis School for Liberal Arts, Pune, India)
* Dr. Subir Bhaduri (smalldesign.in)
* Plus many more!