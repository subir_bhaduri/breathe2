// Importing Bosch's BME280 library
//------------------------------------------------------------------------------------------
#include "SparkFunBME280.h"
BME280 BME;

// Specifi arrays for sampling data
//------------------------------------------------------------------------------------------
float RH_array[array_length];
float T_array[array_length];
float P_array[array_length];

// Initialize BME280
//------------------------------------------------------------------------------------------
void init_BME280() {
  Wire.begin();
  if (BME.beginI2C() == false) //Begin communication over I2C
  {
    DEBUG_PRINTLN(F("The sensor did not respond. Please check wiring."));
  }
  else DEBUG_PRINTLN(F("BME280 success"));

  DEBUG_PRINTLN(F("Initializing BME280..."));
}



// Function to readup latest values of BME280, and store in respective running arrays.
//------------------------------------------------------------------------------------------
void read_BME280(int index) {
  // Store the latest values
  RH_array[index]  = BME.readFloatHumidity();
  T_array[index]   = BME.readTempC();
  P_array[index]   = BME.readFloatPressure();

  DEBUG_PRINT("T now:");
  DEBUG_PRINTLN(BME.readTempC());
}

