// Generic array averaging function.
//------------------------------------------------------------------------------------------
// From: https://forum.arduino.cc/index.php?topic=232508.
float average (float * any_array)  // Array is a float.
{
  float sum = 0 ;  // sum will be larger than an item, long for safety.
  for (int i = 0 ; i < array_length ; i++)
    sum += any_array [i] ;
  return  float(sum / array_length) ;  // average will be fractional, so float may be appropriate.
}





// Compute 'mode' of an array, even if its a floats array
//------------------------------------------------------------------------------------------
// Trick is to divide continious data into 'bins' and then take the bin with max number of members as mode!
// Thanks to Dr. Sumithra S. (SSLA, Pune) for suggesting this method.
float stats_mode (float * any_array)  // NOTE: Array is a float.
{
  // First get min and max of the array
  float array_min = 5000.0;
  float array_max = 0;
  for (int n = 0; n < array_length ; n++) {
    if (array_min > any_array[n])array_min = any_array[n];
    else if (any_array[n] >= array_max) array_max = any_array[n];
  }

  // Compute the intervals
  int intervals = 10;               // # of intervals the array distribution is divided into.
  int frequency_bins[intervals];    // array for classifying values into discrete bins
  float bin_sum[intervals];         // array for summing of members within a bin
  // empty the above arrays
  memset(frequency_bins, 0, sizeof(frequency_bins));
  memset(bin_sum, 0, sizeof(bin_sum));

  // Width of each interval
  float interval_width = (array_max - array_min) / float(intervals);

  // For each of the array members, classify/bin into the right interval
  // Note that this automatically SORTs the array into ascending value in terms of bins
  float lower_bound = 0;
  float upper_bound = 0;
  for ( int n = 0 ; n < array_length ; n++) {
    // interval iterations
    for (int i = 0 ; i < intervals ; i++) {
      //calculate bounds for this interval
      lower_bound = array_min + interval_width * i;
      upper_bound = lower_bound + interval_width;
      if ( ( any_array[n] >= lower_bound ) & (any_array[n] < upper_bound)) {
        frequency_bins[i]++;
        bin_sum[i] += any_array[n];
        break;
      }
    }
  }

  // Get the bin with max population
  // Also since the bins are checked in ascending value, the outcome is:
  // - the bin with max population in case of unimodal distribution OR
  // - the bin with max population AND highest value, in case of multimodal distribution.
  // - and if array is linearly distributed, then again the one with max value is the outcome.
  int max_frequency = 0;
  int max_frequency_bin = 0;
  for (int i = 0 ; i < intervals ; i++) {
    if (frequency_bins[i] > max_frequency) {
      max_frequency = frequency_bins[i];
      max_frequency_bin = i;
    }
  }


  // Take average of the bin with max members = mode.
  float array_mode = bin_sum[max_frequency_bin] / float(max_frequency);


  return  array_mode;
}

