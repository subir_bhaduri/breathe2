/*
    Using Sensirion's SPS30 lib (changed 13th Sept. 2019)
*/
//------------------------------------------------------------------------------------------
// OLD -- library : https://github.com/paulvha/sps30 from Paul van Haastrecht (paulvha@hotmail.com)
// New library from Sensirion - https://github.com/Sensirion/arduino-sps
#include "sps30.h"


// Arrays for storing sampled SPS30 data
//------------------------------------------------------------------------------------------
// Note that global arrays are declared zero if nothing is specified. All the following are global.
// ---- Mass concentration
float PM1_ugm3_array[array_length];
float PM2p5_ugm3_array[array_length];
float PM4_ugm3_array[array_length];
float PM10_ugm3_array[array_length];

// ---- Number concentration
float PM0p5_ncm3_array[array_length];
float PM1_PM0p5_ncm3_array[array_length];
float PM2p5_PM1_ncm3_array[array_length];
float PM4_PM2p5_ncm3_array[array_length];
float PM10_PM4_ncm3_array[array_length];

// Initializing SPS30 sensor (copied from example of SPS30 lib.)
//------------------------------------------------------------------------------------------
void init_SPS30() {
  s16 ret;
  u8 auto_clean_days = 1;
  u32 auto_clean;

  while (sps30_probe() != 0) {
    DEBUG_PRINT(F("SPS sensor probing failed\n"));
    delay(500);
  }

  DEBUG_PRINT(F("SPS sensor probing successful\n"));

  ret = sps30_set_fan_auto_cleaning_interval_days(auto_clean_days);
  if (ret) {
    DEBUG_PRINT(F("error setting the auto-clean interval: "));
    DEBUG_PRINTLN(ret);
  }

  ret = sps30_start_measurement();
  if (ret < 0) {
    DEBUG_PRINT(F("error starting measurement\n"));
  }

  DEBUG_PRINT(F("measurements started\n"));

  delay(1000);
}


// Read PM data once and store in array for box car averaging.
//------------------------------------------------------------------------------------------
void read_SPS30(int index) {
  struct sps30_measurement m;
  char serial[SPS_MAX_SERIAL_LEN];
  u16 data_ready;
  s16 ret;

  do {
    ret = sps30_read_data_ready(&data_ready);
    if (ret < 0) {
      DEBUG_PRINT(F("error reading data-ready flag: "));
      DEBUG_PRINTLN(ret);
    } else if (!data_ready)
      DEBUG_PRINT(F("data not ready, no new measurement available\n"));
    else
      break;
    delay(100); /* retry in 100ms */
  } while (1);

  ret = sps30_read_measurement(&m);
  if (ret < 0) {
    DEBUG_PRINT(F("error reading measurement\n"));
  } else {

    // Mass concentration
    PM1_ugm3_array[index]      = m.mc_1p0;
    PM2p5_ugm3_array[index]    = m.mc_2p5;
    PM4_ugm3_array[index]      = m.mc_4p0;
    PM10_ugm3_array[index]     = m.mc_10p0;

    // Number concentration
    PM0p5_ncm3_array[index]       = m.nc_0p5;
    PM1_PM0p5_ncm3_array[index]   = m.nc_1p0  - m.nc_0p5;
    PM2p5_PM1_ncm3_array[index]   = m.nc_2p5  - m.nc_1p0;
    PM4_PM2p5_ncm3_array[index]   = m.nc_4p0  - m.nc_2p5;
    PM10_PM4_ncm3_array[index]    = m.nc_10p0 - m.nc_4p0;

    DEBUG_PRINT("PM2p5_ugm3:");
    DEBUG_PRINTLN(PM2p5_ugm3_array[index]);
  }
}




