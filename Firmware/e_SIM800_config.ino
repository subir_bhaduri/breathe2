/* Configure device specific or info here..
 *  In this case its 
 *  A) SIM card service provider's data
 *  B) Thingspeak channel APIs for ambient conditions, mass concentrations and number concentrations
 *  These values are used in "f_SIM800_functions" sub-code.
 */
 
// Mention service provider specific APN of your SIM card. 
// "www" for vodafone, "imis/internet" for idea. Apparently www also works for Idea, in India!!
static const char AT_SAPBR_APN[]               PROGMEM = "AT+SAPBR=3,1,\"APN\",\"www\"";

// Enter actual 'write' API keys from thingspeak channels
static const char AT_api_key_BME280[]           PROGMEM = "api_key=__________"; 
static const char AT_api_key_SPS30_Mass[]       PROGMEM = "api_key=__________"; 
static const char AT_api_key_SPS30_Num[]        PROGMEM = "api_key=__________";
