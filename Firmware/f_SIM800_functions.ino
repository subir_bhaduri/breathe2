
// SIM800 Communications
//------------------------------------------------------------------------------------------
//From-https://www.electronicwings.com/arduino/http-client-using-sim900a-gprs-and-arduino-uno
// Required to communicate with SIM800
#include <SoftwareSerial.h>

//Create object named SIM900 of the class SoftwareSerial
// D4 and D3 on Wemos correspond to RX,TX on SIM800L (or vice-versa)
SoftwareSerial gsm(D5, D6);

// This is required for waking up SIM800, but it not used here yet.
const int DTRpin = D7;

// Function to initialize SIM800L module
//----------------------------------------------------------------------------------------------
void init_SIM800() {
  pinMode(DTRpin, OUTPUT);
  digitalWrite(DTRpin, LOW);
  gsm.begin(9600);  /* Define baud rate for software serial communication */
  DEBUG_PRINTLN(F("Initializing SIM800..."));
}


// PROGMEM definitions
//----------------------------------------------------------------------------------------------
// PROGMEM implementation is necessary to save prescious memory which otherwise will be hogged up by strings!!!
// Following style from : https://github.com/holzhey/vooseguro/blob/master/metar.c
// Explanations from SIM800 manual.
// PROGMEM implementation through Arduino IDE for ESP8266 : https://arduino-esp8266.readthedocs.io/en/latest/PROGMEM.html
// Use of simple FPSTR from https://arduino-esp8266.readthedocs.io/en/latest/reference.html?highlight=yield#timing-and-delays


// Normal AT commands for initializations
static const char AT[]                          PROGMEM = "AT";
static const char AT_r_OK[]                     PROGMEM = "OK";

// Enable externded verbose mode
static const char AT_CMEE_2[]                   PROGMEM = "AT_CMEE_2";

// Check if SIM is inserted and ready
static const char AT_CPIN[]                     PROGMEM = "AT+CPIN?";
static const char AT_CPIN_r[]                   PROGMEM = "+CPIN: READY";

// Check if SIM is registered
static const char AT_CREG[]                     PROGMEM = "AT+CREG?";
static const char AT_CREG_r[]                   PROGMEM = "+CREG: 2,1";

// Reset SIM800
static const char AT_CFUN_1_1[]                 PROGMEM = "AT+CFUN=1,1";

// Configure bearer profile
static const char AT_SAPBR_Contype[]            PROGMEM = "AT+SAPBR=3,1,\"Contype\",\"GPRS\"";

// Open GPRS service
static const char AT_SAPBR_1_1[]                PROGMEM = "AT+SAPBR=1,1";

// Get IP address of GPRS, just to check if all is well.
static const char AT_SAPBR_2_1[]                PROGMEM = "AT+SAPBR=2,1";
// Expected reply
static const char AT_SAPBR_2_1_r[]              PROGMEM = "+SAPBR: 1,1,";

// Get location of SIM, lat and long.
static const char AT_CIPGSMLOC_1_1[]            PROGMEM = "AT+CIPGSMLOC=1,1";
// Expected reply
static const char AT_CIPGSMLOC_1_1_r[]          PROGMEM = "+CIPGSMLOC:"; //....longitude,latitude,date,time

// Initialize HTTP service
static const char AT_HTTPINIT[]                 PROGMEM = "AT+HTTPINIT";

// Set parameters for HTTP session
static const char AT_HTTPPARA_CID[]             PROGMEM = "AT+HTTPPARA=\"CID\",1";

// Set parameters for HTTP session
static const char AT_HTTPPARA_URL[]             PROGMEM = "AT+HTTPPARA=\"URL\",\"api.thingspeak.com/update\"";

// POST data of size 100 Bytes with maximum latency time of 10seconds for inputting the data
static const char AT_HTTPDATA[]                 PROGMEM = "AT+HTTPDATA=150,10000";
static const char AT_HTTPDATA_r[]               PROGMEM = "DOWNLOAD";

// POST session start
static const char AT_HTTPACTION[]               PROGMEM = "AT+HTTPACTION=1";
// reply to Post session start, inidicating session has ended successfully
static const char AT_HTTPACTION_r[]             PROGMEM = "+HTTPACTION: 1,200,"; //100 for 100 bytes

// Terminate HTTP session
static const char AT_HTTPTERM[]                 PROGMEM = "AT+HTTPTERM";

// Close GPRS context
static const char AT_SAPBR_0_1[]                PROGMEM = "AT+SAPBR=0,1";

// Put SIM800 in sleep mode
static const char AT_CFUN_0[]                   PROGMEM = "AT+CFUN=0";
//----------------------------------------------------------------------------------------------




// Variables for recording lat/long from GSM SIM module.
volatile float Latitude;
volatile float Longitude;
bool update_latlog_flag = false;



// Generalized buffers for storing PROGMEM strings and useing them easily.
char gen_buf[100];
char response_buffer[100];



// Specify how many tries to do if previous AT interaction failed?
uint8_t max_try_num = 10;
// When to timeout of at SIM800 attempt. A large value is provided here.
unsigned long timeout_value = 10000;


// Function to dump data to thingspeak.com
//----------------------------------------------------------------------------------------------
bool Breathe2_SIM800_thingspeak() {
  // A bool variable to ensure that the previous steps (AT commands may not give deterministic, timely answers)
  // are a success.
  bool answer = true;

  // Check if SIM800 is OK.
  answer = sendATcommand(answer,    FPSTR(AT),                              FPSTR(AT_r_OK),            timeout_value ,  1);

  // Reset SIM800
  answer = sendATcommand(true,      FPSTR(AT_CFUN_1_1),                     FPSTR(AT_r_OK),            timeout_value ,  max_try_num);
  // Give enough time for the SIM800 module to refresh and boot well.
  long_delay(20000);

  // check again if SIM800 is OK.
  answer = sendATcommand(answer,    FPSTR(AT),                              FPSTR(AT_r_OK),            timeout_value ,  max_try_num);

  // Setup the GPRS connection
  answer = sendATcommand(answer,    FPSTR(AT_SAPBR_Contype) ,               FPSTR(AT_r_OK),             timeout_value,  max_try_num);
  answer = sendATcommand(answer,    FPSTR(AT_SAPBR_APN),                    FPSTR(AT_r_OK),             timeout_value,  max_try_num);
  answer = sendATcommand(answer,    FPSTR(AT_SAPBR_1_1),                    FPSTR(AT_r_OK),             timeout_value,  max_try_num);
  answer = sendATcommand(answer,    FPSTR(AT_SAPBR_2_1),                    FPSTR(AT_SAPBR_2_1_r),      timeout_value,  max_try_num);

  // get location
  answer = sendATcommand(answer,    FPSTR(AT_CIPGSMLOC_1_1),                FPSTR(AT_CIPGSMLOC_1_1_r),  timeout_value,  max_try_num);



  //--------------------------------------- SEND DATA to THINGSPEAK Channel by Channel ----------------------------------------------
  //--------- First send BME280 data
  String data = String(FPSTR(AT_api_key_BME280));
  data += "&field1=" + String(average(RH_array));      // RH in %
  data += "&field2=" + String(average(T_array));       // T in deg C
  data += "&field3=" + String(average(P_array));       // P in pascals
  data += "&latitude=" + String(Latitude);                  // Latitude
  data += "&longitude=" + String(Longitude);                // Longitude

  answer = HTTPTransfer(answer, data);



  // ---------------------------- Now SPS30 data
  // First get the index of max PM2.5 mass concentration
  //int max_index = array_max_index(PM2p5_ugm3_array);

  // Mass data
  data = String(FPSTR(AT_api_key_SPS30_Mass));
  data += "&field1=" + String(stats_mode(PM1_ugm3_array));
  data += "&field2=" + String(stats_mode(PM2p5_ugm3_array));
  data += "&field3=" + String(stats_mode(PM4_ugm3_array));
  data += "&field4=" + String(stats_mode(PM10_ugm3_array));

  answer = HTTPTransfer(answer, data);



  // Number concentration data
  data = String(FPSTR(AT_api_key_SPS30_Num));
  data += "&field1=" + String(stats_mode(PM0p5_ncm3_array));
  data += "&field2=" + String(stats_mode(PM1_PM0p5_ncm3_array));
  data += "&field3=" + String(stats_mode(PM2p5_PM1_ncm3_array));
  data += "&field4=" + String(stats_mode(PM4_PM2p5_ncm3_array));
  data += "&field5=" + String(stats_mode(PM10_PM4_ncm3_array));

  answer = HTTPTransfer(answer, data);

  //xxxxxxxxxx End of HTTP transfers ---------------------

  // Shutdown the GPRS service
  answer = sendATcommand(1, FPSTR(AT_SAPBR_0_1),                       FPSTR(AT_r_OK),             timeout_value,  max_try_num);

  // Put SIM800 to minimal functionality mode.
  //answer = sendATcommand(answer, FPSTR(AT_CFUN_0),                          FPSTR(AT_r_OK),             timeout_value,  max_try_num);

  return (answer);
}



// A function to do the actual data dumping process once GPRS and all connections have been established
//----------------------------------------------------------------------------------------------
bool HTTPTransfer(bool answer, String url_data) {
  // Get connected to cloud database
  answer = sendATcommand(answer,    FPSTR(AT_HTTPINIT),                     FPSTR(AT_r_OK),             timeout_value,  max_try_num);
  answer = sendATcommand(answer,    FPSTR(AT_HTTPPARA_CID),                 FPSTR(AT_r_OK),             timeout_value,  max_try_num);
  answer = sendATcommand(answer,    FPSTR(AT_HTTPPARA_URL),                 FPSTR(AT_r_OK),             timeout_value,  max_try_num);

  // Prepare for data transfer
  answer = sendATcommand(answer,    FPSTR(AT_HTTPDATA),                     FPSTR(AT_HTTPDATA_r),       timeout_value,  max_try_num);

  // Data transfer to buffer
  answer = sendATcommand(answer,    url_data,                                FPSTR(AT_r_OK),             timeout_value,  max_try_num);

  // Do the actual transfer command from buffer to thingspeak
  answer = sendATcommand(answer,    FPSTR(AT_HTTPACTION),                   FPSTR(AT_HTTPACTION_r),     timeout_value,  max_try_num);

  // Close the HTTPs service.
  answer = sendATcommand(answer,    FPSTR(AT_HTTPTERM),                     FPSTR(AT_r_OK),             timeout_value, max_try_num);

  return (answer);
}


// A function to 'reliabely' send AT commands to SIM800 and check for expected answers.
//----------------------------------------------------------------------------------------------
// function derived from https://github.com/holzhey/vooseguro/blob/master/metar.c
bool sendATcommand(byte previous_answer, String ATcommand, String expected_answer, unsigned long timeout, uint8_t max_tries) {

  // Something for external indication that the SIM communications have started...
  digitalWrite(BUILTIN_LED, LOW);

  // A generic answer flag to be used in the while loop below
  bool ans = 0;

  // Empty the general buffer
  memset(gen_buf, '\0', sizeof(gen_buf));

  // Load expected answer string into general buffer
  expected_answer.toCharArray(gen_buf, sizeof(gen_buf));

  // check if previous step was a success. if yes only then proceed.
  if (previous_answer != false) {
    uint8_t try_counter = 0; // set a try counter to zero.

    // generic counter to index the reponses
    uint8_t x = 0;

    // timeout related variables
    unsigned long previous = millis();
    unsigned long nowtime = previous;
    bool timeout_bool = false;

    // do this while we get complete expected response. ans==1
    // or do this as long as the number of tries is lower than preset.
    while ((try_counter < max_tries) and (ans == 0)) {
      ESP.wdtFeed();

      // establish a counter to say how many attempts have been made for this AT command
      DEBUG_PRINT(F("try: "));
      DEBUG_PRINTLN(try_counter);
      try_counter++;

      // State the command and its expected reply on Serial monitor
      DEBUG_PRINTLN();
      DEBUG_PRINT("> ATC: ");
      DEBUG_PRINT(ATcommand);
      DEBUG_PRINT(" \t -> Ans: ");
      DEBUG_PRINTLN(expected_answer);

      // Before sending in new command, print and flush GSM data line.
      print_and_flush_gsm_channel();

      // send the actual command to GPS/GPRS channel
      gsm.println(ATcommand);

      // --------------- response subloop
      x = 0;
      previous = millis();
      nowtime = previous;
      timeout_bool = false;

      // do this while responses are coming to Arduino from SIM800
      // do-while loop will also quit if timeouts
      do {
        ESP.wdtFeed();
        if ( gsm.available() != 0) {
          response_buffer[x] =  gsm.read();
          x++;
          //check if reponse matches the expected answer. if yes then answer = 1 and do - while loope will quit.
          if (strstr(response_buffer, gen_buf) != NULL) {
            // if the response matches the expected preset answer, then still collect the complete response
            while ( gsm.available() > 0)  {
              response_buffer[x] = gsm.read();
              x++;
              ESP.wdtFeed(); //yield();
            }
            // set flag as success
            ans = 1;

            // Recover lat long data from SIM reponses. 
            // Use this stage to flag off the next interation of this function and 
            // parse the flushed data for lat/long which otherwise would have been deleted.
            // see ref: https://forum.arduino.cc/index.php?topic=290459.0
            // for finding a string in another string, use 'indexof'...
            if (ATcommand.indexOf("CIPGSMLOC") > 0) update_latlog_flag = true;

            // Print the complete response
            DEBUG_PRINT("# "); DEBUG_PRINTLN(response_buffer);
          }
        }

        // Check if this command timesout. increase timeout value as a response.
        nowtime = millis();
        if ((nowtime - previous) > timeout) {
          timeout_bool = true;
          DEBUG_PRINTLN(F("Timeout"));
        }
        ESP.wdtFeed(); //yield();
      }
      while ((ans == 0) && (timeout_bool == false));
    }

    // delay so that SIM800 communication gets some time to breathe
    long_delay(3000);
    DEBUG_PRINTLN(F("SUCCESS"));
  }
  else {
    ans = 0;
    DEBUG_PRINTLN(F("FAILURE"));
  }

  digitalWrite(BUILTIN_LED, HIGH);
  return (ans);
}


// Function to empty gsm channel of non-useful responses.
//----------------------------------------------------------------------------------------------
void print_and_flush_gsm_channel() {
  // Empty the response buffer
  memset(response_buffer, '\0', sizeof(response_buffer));

  //char a;
  // read all the available characters that come in from gsm  communiation.
  //it think, this clears the read buffer for next commands.
  DEBUG_PRINT(">>");
  int x = 0;
  while ( gsm.available() > 0)  {
    ESP.wdtFeed();
    response_buffer[x] = gsm.read();
    x++;
  }
  DEBUG_PRINT(response_buffer);
  DEBUG_PRINTLN("<<");

  if (update_latlog_flag == true) {
    update_latlong(response_buffer);
    update_latlog_flag = false;
  }

  // Empty the response buffer
  memset(response_buffer, '\0', sizeof(response_buffer));
}

//Function to parse lat/long data from SIM responses
//----------------------------------------------------------------------------------------------
// ref: https://forum.arduino.cc/index.php?topic=288234.0
void update_latlong(char* receivedChars) {
  char * strtokIndx; // this is used by strtok() as an index

  DEBUG_PRINTLN(receivedChars);

  strtokIndx = strtok(receivedChars, ",");      // get the first part - the string

  // skip it as its zeor for now
  //strcpy(empty, strtokIndx);                    // copy it to messageFromPC

  // this continues where the previous call left off
  strtokIndx = strtok(NULL, ",");
  // convert this part to a float
  Longitude = atof(strtokIndx);

  // this continues where the previous call left off
  strtokIndx = strtok(NULL, ",");
  // convert this part to a float
  Latitude = atof(strtokIndx);

  DEBUG_PRINT("---- Lat:");
  DEBUG_PRINT(Latitude);
  DEBUG_PRINT(",");
  DEBUG_PRINT("Long:"); DEBUG_PRINTLN(Longitude);
}

